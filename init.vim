set tabstop=2
set softtabstop=2
set shiftwidth=2
set noexpandtab
augroup project
  autocmd!
  autocmd BufRead,BufNewFile *.h,*.c set filetype=c
  autocmd BufNewFile,BufRead *.hpp,*.cpp set filetype=cpp
augroup END

let &path.="../include"

set makeprg=make\ -C\ ../build\ -j9
nnoremap <F4> :make!<cr>

set number
syntax on

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 0
let g:syntastic_check_on_wq = 0

call plug#begin()

Plug 'rust-lang/rust.vim'
Plug 'jiangmiao/auto-pairs'
Plug 'bfrg/vim-cpp-modern'
Plug 'scrooloose/syntastic'
Plug 'itchyny/lightline.vim'

call plug#end()
